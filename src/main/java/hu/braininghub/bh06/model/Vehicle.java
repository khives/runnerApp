package hu.braininghub.bh06.model;

public enum Vehicle {

	CAR, BIKE, MOTORBIKE;
}
