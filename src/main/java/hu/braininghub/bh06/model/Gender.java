package hu.braininghub.bh06.model;

public enum Gender {

	MALE, FEMALE, OTHER;
}
